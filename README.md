# library

Tecnologias:
- Python3.7
- Django 2.2
- Django Rest Framework
- Angular 8
- Base de Datos SQLite

Backend:
- Debe iniciar un ambiente virtual en python3 
- En la carpeta del backend -> library se debe instalar los requermientos que se describen en requeriments.txt
- Despues de este debe correr el servidor django "python manage.py runserver"
Frontend:
- En la carpeta front debe tener node version 12
- Angular 8
- debe instalar las dependencias con npm install.
- debe correr el servidor con "ng serve"
- observar la ruta http://localhost:4200/login en el navegador para observar la aplicacion en funcionamiento


usuario: admin
contraseña: inkalabs
