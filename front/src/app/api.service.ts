import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseurl = "http://127.0.0.1:8000/api";
  httpHeaders = new HttpHeaders({'Content-Type':'application/json'});
  constructor(private http: HttpClient) { }
  getAllBooks(): Observable<any>{
    return this.http.get(this.baseurl+'/book/select/', 
    {headers: this.httpHeaders});
  }
  getOneBook(id): Observable<any>{
    return this.http.get(this.baseurl+'/book/select/'+id+'/', 
    {headers: this.httpHeaders});
  }
  updateBook(book): Observable<any>{
    const body = {id: book.id,title:book.title,author:book.author,year_publication:book.year_publication,edition:book.edition,number_copies:book.number_copies};
    return this.http.put(this.baseurl+'/book/update/'+book.id+'/', body, 
    {headers: this.httpHeaders});
  }
  createBook(book): Observable<any>{
    const body = {id: book.id,title:book.title,author:book.author,year_publication:book.year_publication,edition:book.edition,number_copies:book.number_copies,cover_image:book.cover_image};
    return this.http.post(this.baseurl+'/book/insert/', body, 
    {headers: this.httpHeaders});
  }
  deleteBook(id): Observable<any>{
    return this.http.delete(this.baseurl+'/book/delete/'+id+'/', 
    {headers: this.httpHeaders});
  }
}
