import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }
  baseurl = "http://127.0.0.1:8000/api";
  httpHeaders = new HttpHeaders({'Content-Type':'application/json'});
  getUserDetails(username,password)
  {
    return this.http.post(this.baseurl+'/login/',{username,password}, 
    {headers: this.httpHeaders}).subscribe(data => {
        this.router.navigateByUrl('/home');
    });
  }
}
