import { Component } from '@angular/core';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [ApiService]
})
export class AppComponent {
  books = [{title:'test'}];
  selectedBook;
  constructor(private api:ApiService){
    this.getAllBooks();
    this.selectedBook = {id:-1,title:'t',author:'t',year_publication:'t',edition:'t',number_copies:'t'};
  }
  getAllBooks = () =>{
    this.api.getAllBooks().subscribe(
      data =>{
        this.books= data;
      },
      error =>{
        console.log(error);
      }
    );
  }

  bookClicked = (book) =>{
    this.api.getOneBook(book.id).subscribe(
      data =>{
        this.selectedBook= data;
      },
      error =>{
        console.log(error);
      }
    );
  }
  updateBook = () => { 
    this.api.updateBook(this.selectedBook).subscribe(
      data =>{
        this.getAllBooks();
      },
      error =>{
        console.log(error);
      }
    );
  }
  createBook = () => { 
    this.api.createBook(this.selectedBook).subscribe(
      data =>{
        this.books.push(data);
      },
      error =>{
        console.log(error);
      }
    );
  }
  deleteBook = () => { 
    this.api.deleteBook(this.selectedBook.id).subscribe(
      data =>{
        this.getAllBooks();
      },
      error =>{
        console.log(error);
      }
    );
  }
}
