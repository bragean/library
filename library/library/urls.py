from django.contrib import admin
from django.urls import path, include ,re_path
from django.conf.urls.static import static
from django.conf import settings
api_urls = [
    re_path(r'^', include('book.urls')),
]

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^api/', include(api_urls)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
