from rest_framework import serializers
from .models import *

# Serializar for Books
class BookSerializer(serializers.ModelSerializer):
    class Meta:
        model = Book
        fields = '__all__'
