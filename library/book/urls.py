from django.conf.urls import url
from .views import *

urlpatterns = [
    url(r'^login/$', login, name="login_user"),
    url(r'^book/insert/$', BookCreate.as_view(), name="insert_book"),
    url(r'^book/update/(?P<pk>[0-9]+)/$', BookUpdate.as_view(), name="update_book"),
    url(r'^book/delete/(?P<pk>[0-9]+)/$', BookUpdate.as_view(), name="delete_book"),
    url(r'^book/select/(?P<pk>[0-9]+)/$', BookUpdate.as_view(), name="delete_book"),
    url(r'^book/select/$', BookCreate.as_view(), name="delete_book"),

]