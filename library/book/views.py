from django.shortcuts import render
from .models import *
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from .serializer import *
from datetime import *
import json
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
from django.contrib import auth

from django.views.decorators.csrf import csrf_exempt
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)

# Class for create and list Books
class BookCreate(ListCreateAPIView):
    serializer_class = BookSerializer
    def get_queryset(self):
        return Book.objects.all().order_by('created')

# Class for update and delete Books
class BookUpdate(RetrieveUpdateDestroyAPIView):
    serializer_class = BookSerializer
    queryset = Book.objects.all().order_by('created')

# class to authenticate username and password
@csrf_exempt
@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
	username = request.data.get("username")
	password = request.data.get("password")
	if username is None or password is None:
		return Response({'error': 'Porfavor insertar username y password'},
					status=HTTP_400_BAD_REQUEST)
	user = authenticate(username=username, password=password)
	if not user:
		return Response({'error': 'Validacion Incorrecta'},
						status=HTTP_404_NOT_FOUND)
	token, _ = Token.objects.get_or_create(user=user)
	return Response({'sucess':'true'},
					status=HTTP_200_OK)