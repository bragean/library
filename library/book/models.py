from django.db import models
# Create your models here.

# Model for Books 
class Book(models.Model):
    title = models.CharField(max_length=500)
    author = models.CharField(max_length=250, blank= True, null=True)
    year_publication = models.CharField(max_length=10, blank=True, null=True)
    edition = models.CharField(max_length=10, blank=True, null=True)
    cover_image = models.ImageField(upload_to='images', blank=True, null=True)
    number_copies = models.CharField(max_length=10,blank=True, null=True)
    status = models.CharField(max_length=1, default='1')
    created = models.DateTimeField(auto_now_add=True, null=True)
